// import arrayOfObjects from './array-of-objects'



window.onload = function() {
  // Массив для страницы с нормативами 
  const iBeamSectors = [
    {            _id: '20_93',      title: 'СТО АСЧМ 20-93'
    },    {      _id: '257837',     title: 'ГОСТ Р 57837-2017'
    },    {      _id: '26020',      title: 'ГОСТ 26020-83'
    },    {      _id: '8239',       title: 'ГОСТ 8239-89'
    },    {      _id: '19425',      title: 'ГОСТ 19425-74'
    },    {      _id: '1025',       title: 'DIN 1025'
    },
    
  ]

  
  //  Массив с Двутаврами
  const iBeam_20_93 = [
    {
      _id: '20_93_056',
      kind: '100Ш1',
      standart: '20-93',
      result: { ptm: '9,249', rm: '3,176', rt: '13,773' }
    },
    {
      _id: '20_93_053',
      kind: '100Ш2',
      standart: '20-93',
      result: { ptm: '10,308', rm: '3,190', rt: '12,358' }
    },
    {
      _id: '20_93_054',
      kind: '100Ш3',
      standart: '20-93',
      result: { ptm: '11,358', rm: '3,204', rt: '11,216' }
    },
    {
      _id: '20_93_065',
      kind: '100Ш4',
      standart: '20-93',
      result: { ptm: '12,458', rm: '3,215', rt: '10,225' }
    },
    {
      _id: '20_93_03',
      kind: '10Б1',
      standart: '20-93',
      result: { ptm: '2,582', rm: '0,400', rt: '49,333' }
    },
    {
      _id: '20_93_02',
      kind: '12Б1',
      standart: '20-93',
      result: { ptm: '2,339', rm: '0,472', rt: '54,465' }
    },
    {
      _id: '20_93_01',
      kind: '12Б2',
      standart: '20-93',
      result: { ptm: '2,780', rm: '0,475', rt: '45,823' }
    },
    {
      _id: '20_93_05',
      kind: '14Б1',
      standart: '20-93',
      result: { ptm: '2,447', rm: '0,547', rt: '52,049' }
    },
    {
      _id: '20_93_00',
      kind: '14Б2',
      standart: '20-93',
      result: { ptm: '2,983', rm: '0,551', rt: '42,699' }
    },
    {
      _id: '20_93_04',
      kind: '16Б1',
      standart: '20-93',
      result: { ptm: '2,616', rm: '0,619', rt: '48,702' }
    },
    {
      _id: '20_93_011',
      kind: '16Б2',
      standart: '20-93',
      result: { ptm: '3,227', rm: '0,623', rt: '39,473' }
    },
    {
      _id: '20_93_08',
      kind: '18Б1',
      standart: '20-93',
      result: { ptm: '2,821', rm: '0,694', rt: '45,155' }
    },
    {
      _id: '20_93_014',
      kind: '18Б2',
      standart: '20-93',
      result: { ptm: '3,431', rm: '0,698', rt: '37,128' }
    },
    {
      _id: '20_93_07',
      kind: '20Б1',
      standart: '20-93',
      result: { ptm: '3,527', rm: '0,770', rt: '36,122' }
    },
    {
      _id: '20_93_059',
      kind: '20К1',
      standart: '20-93',
      result: { ptm: '4,571', rm: '1,153', rt: '27,868' }
    },
    {
      _id: '20_93_063',
      kind: '20К2',
      standart: '20-93',
      result: { ptm: '5,469', rm: '1,162', rt: '23,293' }
    },
    {
      _id: '20_93_031',
      kind: '20Ш1',
      standart: '20-93',
      result: { ptm: '4,091', rm: '0,954', rt: '31,142' }
    },
    {
      _id: '20_93_010',
      kind: '25Б1',
      standart: '20-93',
      result: { ptm: '3,399', rm: '0,961', rt: '37,480' }
    },
    {
      _id: '20_93_06',
      kind: '25Б2',
      standart: '20-93',
      result: { ptm: '3,893', rm: '0,967', rt: '32,727' }
    },
    {
      _id: '20_93_062',
      kind: '25К1',
      standart: '20-93',
      result: { ptm: '5,519', rm: '1,445', rt: '23,084' }
    },
    {
      _id: '20_93_072',
      kind: '25К2',
      standart: '20-93',
      result: { ptm: '6,337', rm: '1,455', rt: '20,101' }
    },
    {
      _id: '20_93_058',
      kind: '25К3',
      standart: '20-93',
      result: { ptm: '6,988', rm: '1,463', rt: '18,229' }
    },
    {
      _id: '20_93_036',
      kind: '25Ш1',
      standart: '20-93',
      result: { ptm: '4,905', rm: '1,147', rt: '25,971' }
    },
    {
      _id: '20_93_09',
      kind: '30Б1',
      standart: '20-93',
      result: { ptm: '3,521', rm: '1,159', rt: '36,177' }
    },
    {
      _id: '20_93_015',
      kind: '30Б2',
      standart: '20-93',
      result: { ptm: '4,017', rm: '1,165', rt: '31,715' }
    },
    {
      _id: '20_93_057',
      kind: '30К1',
      standart: '20-93',
      result: { ptm: '6,357', rm: '1,743', rt: '20,040' }
    },
    {
      _id: '20_93_068',
      kind: '30К2',
      standart: '20-93',
      result: { ptm: '6,848', rm: '1,749', rt: '18,602' }
    },
    {
      _id: '20_93_064',
      kind: '30К3',
      standart: '20-93',
      result: { ptm: '7,662', rm: '1,759', rt: '16,626' }
    },
    {
      _id: '20_93_073',
      kind: '30К4',
      standart: '20-93',
      result: { ptm: '7,664', rm: '1,759', rt: '16,621' }
    },
    {
      _id: '20_93_034',
      kind: '30Ш1',
      standart: '20-93',
      result: { ptm: '5,397', rm: '1,341', rt: '23,603' }
    },
    {
      _id: '20_93_035',
      kind: '30Ш2',
      standart: '20-93',
      result: { ptm: '6,448', rm: '1,355', rt: '19,755' }
    },
    {
      _id: '20_93_013',
      kind: '35Б1',
      standart: '20-93',
      result: { ptm: '3,897', rm: '1,352', rt: '32,691' }
    },
    {
      _id: '20_93_019',
      kind: '35Б2',
      standart: '20-93',
      result: { ptm: '4,636', rm: '1,362', rt: '27,477' }
    },
    {
      _id: '20_93_069',
      kind: '35К1',
      standart: '20-93',
      result: { ptm: '6,877', rm: '2,022', rt: '18,523' }
    },
    {
      _id: '20_93_070',
      kind: '35К2',
      standart: '20-93',
      result: { ptm: '8,516', rm: '2,042', rt: '14,958' }
    },
    {
      _id: '20_93_030',
      kind: '35Ш1',
      standart: '20-93',
      result: { ptm: '5,154', rm: '1,614', rt: '24,715' }
    },
    {
      _id: '20_93_039',
      kind: '35Ш2',
      standart: '20-93',
      result: { ptm: '6,237', rm: '1,628', rt: '20,425' }
    },
    {
      _id: '20_93_016',
      kind: '40Б1',
      standart: '20-93',
      result: { ptm: '4,666', rm: '1,547', rt: '27,303' }
    },
    {
      _id: '20_93_022',
      kind: '40Б2',
      standart: '20-93',
      result: { ptm: '5,404', rm: '1,557', rt: '23,572' }
    },
    {
      _id: '20_93_075',
      kind: '40К1',
      standart: '20-93',
      result: { ptm: '8,052', rm: '2,320', rt: '15,822' }
    },
    {
      _id: '20_93_067',
      kind: '40К2',
      standart: '20-93',
      result: { ptm: '9,361', rm: '2,336', rt: '13,608' }
    },
    {
      _id: '20_93_071',
      kind: '40К3',
      standart: '20-93',
      result: { ptm: '10,826', rm: '2,354', rt: '11,767' }
    },
    {
      _id: '20_93_074',
      kind: '40К4',
      standart: '20-93',
      result: { ptm: '12,442', rm: '2,374', rt: '10,239' }
    },
    {
      _id: '20_93_066',
      kind: '40К5',
      standart: '20-93',
      result: { ptm: '15,605', rm: '2,374', rt: '8,163' }
    },
    {
      _id: '20_93_032',
      kind: '40Ш1',
      standart: '20-93',
      result: { ptm: '5,927', rm: '1,905', rt: '21,494' }
    },
    {
      _id: '20_93_037',
      kind: '40Ш2',
      standart: '20-93',
      result: { ptm: '7,073', rm: '1,922', rt: '18,011' }
    },
    {
      _id: '20_93_027',
      kind: '45Б1',
      standart: '20-93',
      result: { ptm: '5,137', rm: '1,641', rt: '24,799' }
    },
    {
      _id: '20_93_012',
      kind: '45Б2',
      standart: '20-93',
      result: { ptm: '5,860', rm: '1,651', rt: '21,737' }
    },
    {
      _id: '20_93_040',
      kind: '45Ш1',
      standart: '20-93',
      result: { ptm: '7,804', rm: '2,017', rt: '16,324' }
    },
    {
      _id: '20_93_021',
      kind: '50Б1',
      standart: '20-93',
      result: { ptm: '5,346', rm: '1,728', rt: '23,830' }
    },
    {
      _id: '20_93_024',
      kind: '50Б2',
      standart: '20-93',
      result: { ptm: '5,835', rm: '1,736', rt: '21,832' }
    },
    {
      _id: '20_93_023',
      kind: '50Б3',
      standart: '20-93',
      result: { ptm: '6,544', rm: '1,746', rt: '19,467' }
    },
    {
      _id: '20_93_038',
      kind: '50Ш1',
      standart: '20-93',
      result: { ptm: '6,938', rm: '2,097', rt: '18,360' }
    },
    {
      _id: '20_93_033',
      kind: '50Ш2',
      standart: '20-93',
      result: { ptm: '8,396', rm: '2,100', rt: '15,173' }
    },
    {
      _id: '20_93_041',
      kind: '50Ш3',
      standart: '20-93',
      result: { ptm: '9,423', rm: '2,110', rt: '13,519' }
    },
    {
      _id: '20_93_043',
      kind: '50Ш4',
      standart: '20-93',
      result: { ptm: '10,441', rm: '2,120', rt: '12,201' }
    },
    {
      _id: '20_93_017',
      kind: '55Б1',
      standart: '20-93',
      result: { ptm: '5,948', rm: '1,906', rt: '21,416' }
    },
    {
      _id: '20_93_020',
      kind: '55Б2',
      standart: '20-93',
      result: { ptm: '6,522', rm: '1,913', rt: '19,533' }
    },
    {
      _id: '20_93_018',
      kind: '60Б1',
      standart: '20-93',
      result: { ptm: '6,240', rm: '1,930', rt: '20,413' }
    },
    {
      _id: '20_93_025',
      kind: '60Б2',
      standart: '20-93',
      result: { ptm: '6,928', rm: '1,940', rt: '18,388' }
    },
    {
      _id: '20_93_042',
      kind: '60Ш1',
      standart: '20-93',
      result: { ptm: '7,613', rm: '2,292', rt: '16,733' }
    },
    {
      _id: '20_93_047',
      kind: '60Ш2',
      standart: '20-93',
      result: { ptm: '9,461', rm: '2,298', rt: '13,464' }
    },
    {
      _id: '20_93_044',
      kind: '60Ш3',
      standart: '20-93',
      result: { ptm: '10,925', rm: '2,310', rt: '11,660' }
    },
    {
      _id: '20_93_045',
      kind: '60Ш4',
      standart: '20-93',
      result: { ptm: '12,375', rm: '2,322', rt: '10,294' }
    },
    {
      _id: '20_93_026',
      kind: '70Б0',
      standart: '20-93',
      result: { ptm: '6,829', rm: '2,241', rt: '18,654' }
    },
    {
      _id: '20_93_029',
      kind: '70Б1',
      standart: '20-93',
      result: { ptm: '6,990', rm: '2,357', rt: '18,224' }
    },
    {
      _id: '20_93_028',
      kind: '70Б2',
      standart: '20-93',
      result: { ptm: '7,756', rm: '2,368', rt: '16,425' }
    },
    {
      _id: '20_93_050',
      kind: '70Ш1',
      standart: '20-93',
      result: { ptm: '8,426', rm: '2,510', rt: '15,118' }
    },
    {
      _id: '20_93_055',
      kind: '70Ш2',
      standart: '20-93',
      result: { ptm: '9,632', rm: '2,518', rt: '13,225' }
    },
    {
      _id: '20_93_061',
      kind: '70Ш3',
      standart: '20-93',
      result: { ptm: '11,427', rm: '2,530', rt: '11,148' }
    },
    {
      _id: '20_93_060',
      kind: '70Ш4',
      standart: '20-93',
      result: { ptm: '12,963', rm: '2,541', rt: '9,827' }
    },
    {
      _id: '20_93_052',
      kind: '70Ш5',
      standart: '20-93',
      result: { ptm: '14,699', rm: '2,556', rt: '8,667' }
    },
    {
      _id: '20_93_048',
      kind: '80Ш1',
      standart: '20-93',
      result: { ptm: '7,799', rm: '2,689', rt: '16,334' }
    },
    {
      _id: '20_93_051',
      kind: '80Ш2',
      standart: '20-93',
      result: { ptm: '8,990', rm: '2,708', rt: '14,170' }
    },
    {
      _id: '20_93_046',
      kind: '90Ш1',
      standart: '20-93',
      result: { ptm: '8,471', rm: '2,880', rt: '15,038' }
    },
    {
      _id: '20_93_049',
      kind: '90Ш2',
      standart: '20-93',
      result: { ptm: '9,347', rm: '2,898', rt: '13,629' }
    }
  ]

  

      
// Переход на скрин с нармитивами
  document.getElementById('s1_one').onclick = function () {
    norms()
       
    // Переход на скрин с изделиями
    document.getElementById('20_93').onclick = function () {

      norm_20_93()

      // Выбор и поиск id изделия
      let kind = document.querySelectorAll('.kind')
      kind.forEach(t => {
        t.addEventListener('click', function() {
            let kind_id = t.getAttribute('id')
             document.getElementById(kind_id).onclick = function () {

               const metal = iBeam_20_93.find(p => p._id === kind_id)
               
               
               // Рачетные величины 
              const ptm = metal.result.ptm, /*Приведенная толщина металла */
                    rm = metal.result.rm,   /*Площадь поверхности / 1 м */
                    rt = metal.result.rt    /*Площадь поверхности / 1 т */



              console.log(ptm)
            }
          })
        })





    }
  }

  // Рендер скрина с нормативами
  function norms() {
    let norms =''
    for (key in iBeamSectors) {
      norms += `

      <div class="title" id="${iBeamSectors[key]._id}">${iBeamSectors[key].title }</div>

      `
    }
    document.getElementById('sector_1').innerHTML = norms
  }
  

  // Рендер скрина с изделиями 
  function norm_20_93() { 
    let norm_20_93 =''
    for (key in iBeam_20_93) {
      norm_20_93 += `
      
        <div class="kind" id="${iBeam_20_93[key]._id}">${iBeam_20_93[key].kind }</div>
      
      `
    }
    document.getElementById('sector_1').innerHTML = `<div class="sector_2 conteiner">${norm_20_93}</div>`
  }

  // Выбор огнезащитный краски 
  
  


}

